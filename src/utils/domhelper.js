import { element, SmplElement } from '../elements/element.js';

//Original Code from colxi https://github.com/colxi/getEventListeners/
Element.prototype._addEventListener = Element.prototype.addEventListener;
Element.prototype.addEventListener = function (type, listener, useCapture = false) {
	// declare listener
	this._addEventListener(type, listener, useCapture);

	if (!this.eventListenerList) this.eventListenerList = {};
	if (!this.eventListenerList[type]) this.eventListenerList[type] = [];

	// add listener to  event tracking list
	this.eventListenerList[type].push(listener);
};
Element.prototype.getEventListeners = function (type) {
	if (!this.eventListenerList) this.eventListenerList = {};

	// return reqested listeners type or all them
	if (type === undefined) return this.eventListenerList;
	return this.eventListenerList[type];
};

/**
 * Finds an HTML element based on the specified element.
 * @param {HTMLElement|SmplElement|string} element - The element to find.
 * @returns {HTMLElement} The found HTML element.
 */
export function findElement(element) {
	var htmlElement;
	if (element instanceof SmplElement) {
		htmlElement = element.getDomElement();
	} else if (typeof element === 'string' && element.startsWith('#')) {
		htmlElement = document.getElementById(element.substring(1));
	} else if (typeof element === 'string' && element.startsWith('.')) {
		htmlElement = document.getElementsByClassName(element.substring(1)) ?? [];
		htmlElement = htmlElement.length ? htmlElement[0] : null;
	} else if (typeof element === 'string') {
		htmlElement = document.getElementsByTagName(element) ?? [];
		htmlElement = htmlElement.length ? htmlElement[0] : null;
	} else if (element instanceof HTMLElement) {
		return element;
	}
	return htmlElement;
}

/**
 * Appends the specified new element to the specified element.
 * @param {HTMLElement} newElement - The element to append.
 * @param {HTMLElement|SmplElement|string} element - The element to append to.
 */
export function append(newElement, element) {
	const htmlElement = findElement(element);
	newElement = findElement(newElement);
	if (htmlElement && newElement) {
		htmlElement.appendChild(newElement);
	}
}

/**
 * Inserts the specified new element after the specified element.
 * @param {HTMLElement} newElement - The element to insert.
 * @param {HTMLElement|SmplElement|string} element - The element to insert after.
 * @returns {HTMLElement} The element being inserted.
 */
export function insertAfter(newElement, element) {
	const htmlElement = findElement(element);
	newElement = findElement(newElement);
	if (htmlElement && newElement) {
		htmlElement.parentNode.insertBefore(newElement, htmlElement.nextSibling);
	}
	return this;
}

/**
 * Inserts the specified element into the DOM as the first child of the specified parent element.
 * @param {HTMLElement} newElement - The element to insert.
 * @param {HTMLElement} element - The parent element to insert the element into.
 */
export function prepend(newElement, element) {
	const htmlElement = findElement(element);
	newElement = findElement(newElement);
	if (htmlElement && newElement) {
		htmlElement.insertBefore(newElement, htmlElement.firstChild);
	}
}

/**
 * Inserts the specified new element before the specified element.
 * @param {HTMLElement} newElement - The element to insert.
 * @param {HTMLElement|SmplElement|string} element - The element to insert before.
 * @returns {HTMLElement} The element being inserted.
 */
export function insertBefore(newElement, element) {
	const htmlElement = findElement(element);
	newElement = findElement(newElement);
	if (htmlElement && newElement) {
		htmlElement.parentNode.insertBefore(newElement, htmlElement);
	}
	return this;
}

/**
 * Removes all child elements from the specified element.
 * @param {HTMLElement|SmplElement|string} element - The element to remove the child elements from.
 */
export function removeChildren(element) {
	element = findElement(element);
	while (element.firstChild) {
		element.removeChild(element.lastChild);
	}
}

/**
 * Converts an HTML element to a SmplElement.
 * @param {HTMLElement} htmlElement - The HTML element to convert.
 * @returns {SmplElement} The converted SmplElement.
 */
export function toSmplElement(htmlElement) {
	const styles = htmlElement.style,
		classes = htmlElement.className.split(' '),
		attributes = Object.fromEntries(Array.from(htmlElement.attributes).map((item) => [item.name, item.value])),
		events = htmlElement.getEventListeners();
	return element(htmlElement.tagName, {
		styles: styles,
		class: classes,
		attributes: attributes,
		events: events,
	})
		.id(htmlElement.id)
		.remove();
}
