import { SmplElement } from '../element.js';

export class Script extends SmplElement {
	constructor(options) {
		super('script', options);
	}
}

export function script(options) {
	return new Script(options);
}
