import { SmplElement } from '../element.js';

export class Canvas extends SmplElement {
	constructor(options) {
		super('canvas', options);
	}
}

export function canvas(options) {
	return new Canvas(options);
}
