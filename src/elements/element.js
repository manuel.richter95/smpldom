'use strict';

import { append, insertAfter, insertBefore, prepend, removeChildren } from '../utils/domhelper.js';

/**
 * SmplElement is a class that represents an HTML element. It provides methods for creating and manipulating HTML elements.
 */
export class SmplElement {
	/**
	 * Constructs a new SmplElement.
	 * @param {string} type - The type of HTML element to create (e.g. 'div', 'p', etc.)
	 * @param {Object} [options] - Options for the element.
	 * @param {string} [options.class] - The class(es) to assign to the element.
	 * @param {Object} [options.attributes] - The attributes to assign to the element.
	 * @param {Object} [options.styles] - The styles to assign to the element.
	 * @param {Object} [options.events] - The events to bind to the element.
	 */
	constructor(type, options) {
		this._text = null;
		this._type = type;
		this._options = options;
		this._class = options?.class;
		this._attributes = options?.attributes;
		this._id = options?.attributes?.id;
		this._name = options?.attributes?.name;
		this._styles = options?.styles;
		this._events = options?.events;
		this._domElement = null;
		this.append(document.body);
	}

	/**
	 * Appends the current element to the given element.
	 * @param {HTMLElement|SmplElement|string} [element=document.body] - The element to append the current element to.
	 * @returns {SmplElement} The current element.
	 */
	append(element = document.body) {
		append(this._getOrCreateHtmlElement(), element);
		return this;
	}

	/**
	 * Preprends the current element to the given element.
	 * @param {HTMLElement|SmplElement|string} - The element to prepend the current element to.
	 * @returns {SmplElement} The current element.
	 */
	prepend(element) {
		prepend(this._getOrCreateHtmlElement, element);
		return this;
	}

	/**
	 * Inserts the current element after the given element.
	 * @param {HTMLElement|SmplElement|string} element - The element to insert the current element after.
	 * @returns {SmplElement} The current element.
	 */
	insertAfter(element) {
		insertAfter(this._getOrCreateHtmlElement(), element);
		return this;
	}

	/**
	 * Inserts the current element before the given element.
	 * @param {HTMLElement|SmplElement|string} element - The element to insert the current element before.
	 * @returns {SmplElement} The current element.
	 */
	insertBefore(element) {
		insertBefore(this._getOrCreateHtmlElement(), element);
		return this;
	}

	/**
	 * Inserts a line break element after the current element.
	 * @returns {SmplElement} The current element.
	 */
	br() {
		element('br').insertAfter(this);
		return this;
	}

	/**
	 * Hides the current element.
	 * @returns {SmplElement} The current element.
	 */
	hide() {
		this._domElement.style.visibility = 'hidden';
		return this;
	}

	/**
	 * Shows the current element.
	 * @returns {SmplElement} The current element.
	 */
	show() {
		this._domElement.style.visibility = 'visible';
		return this;
	}

	/**
	 * Removes the current element from the DOM.
	 * @returns {SmplElement} The current element.
	 */
	remove() {
		this._domElement.remove();
		return this;
	}

	/**
	 * Sets a CSS attribute on the current element.
	 * @param {Object|string} attribute - The attribute to set. If an object is passed, each key/value pair will be set as a CSS attribute. If a string is passed, it is expected to be the name of the attribute to set, and the second argument should be the value.
	 * @param {string} [value] - The value to set for the attribute.
	 * @returns {SmplElement} The current element.
	 */
	css(attribute, value) {
		const domElement = this._getOrCreateHtmlElement();
		if (typeof attribute === 'object') {
			Object.assign(domElement.style, attribute);
		} else if (typeof attribute === 'string') {
			domElement.style[attribute] = value;
		}
		return this;
	}

	/**
	 * Sets the 'id' attribute of the current element.
	 * @param {string} id - The value to set for the 'id' attribute.
	 * @returns {SmplElement} The current element.
	 */
	id(id) {
		this._id = id;
		return this.attr('id', id);
	}

	/**
	 * Sets the 'name' attribute of the current element.
	 * @param {string} name - The value to set for the 'name' attribute.
	 * @returns {SmplElement} The current element.
	 */
	name(name) {
		this._name = name;
		return this.attr('name', name);
	}

	/**
	 * Sets an attribute on the current element.
	 * @param {Object|string} attribute - The attribute to set. If an object is passed, each key/value pair will be set as an attribute. If a string is passed, it is expected to be the name of the attribute to set, and the second argument should be the value.
	 * @param {string} [value] - The value to set for the attribute.
	 * @returns {SmplElement} The current element.
	 */
	attr(attribute, value) {
		const domElement = this._getOrCreateHtmlElement();
		if (Array.isArray(attribute)) {
			//set key without value
			attribute.forEach((key) => domElement.setAttribute(key, ''));
		} else if (typeof attribute === 'object') {
			for (let key in attribute) {
				console.log(key);
				value = attribute[key];
				domElement.setAttribute(key, value ?? '');
			}
		} else if (typeof attribute === 'string') {
			domElement.setAttribute(attribute, value ?? '');
		}
		return this;
	}

	/**
	 * Adds the given element(s) to the current element.
	 * @param {SmplElement|SmplElement[]} element - The element(s) to add.
	 * @returns {SmplElement} The current element.
	 */
	add(...element) {
		if (Array.isArray(element)) {
			element.forEach((el) => this._addElement(el));
		} else if (element instanceof SmplElement) {
			this._addElement(element);
		}
		return this;
	}

	/**
	 * Adds a label element to the current element, with the given text and options.
	 * @param {string} text - The text to display in the label element.
	 * @param {Object} [options] - Options for the label element.
	 * @returns {SmplElement} The current element.
	 */
	label(text, options) {
		//TODO: always insert at first position
		return text ? this.add(element('label', options).text(text)) : this;
	}

	/**
	 * Sets the text content of the current element.
	 * @param {string} text - The text to set as the content of the element.
	 * @returns {SmplElement} The current element.
	 */
	text(text) {
		text = text
			? this.html(text.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, '&quot;').replace(/'/g, '&#039;'))
			: '';
		return this;
	}

	/**
	 * Sets the 'width' style of the current element.
	 * @param {string} width - The value to set for the 'width' style.
	 * @returns {SmplElement} The current element.
	 */
	width(width) {
		return this.css('width', width);
	}

	/**
	 * Sets the 'height' style of the current element.
	 * @param {string} height - The value to set for the 'height' style.
	 * @returns {SmplElement} The current element.
	 */
	height(height) {
		return this.css('height', height);
	}

	/**
	 * Sets the 'text-align' style of the current element.
	 * @param {string} align - The value to set for the 'align' style.
	 * @returns {SmplElement} The current element.
	 */
	textAlign(align) {
		return this.css('text-align', align);
	}

	/**
	 * Sets the 'value' of the current element.
	 * @param {string|number} value - The value to set for the 'value' style.
	 * @returns {SmplElement} The current element.
	 */
	value(value) {
		return this.attr('value', value);
	}

	/**
	 * Centers the current element horizontally within its parent element.
	 * @returns {SmplElement} The current element.
	 */
	center() {
		return this.css({
			width: this._domElement.style.width || 0,
			margin: '0 auto',
		});
	}

	/**
	 * Aligns the current element to the right within its parent element.
	 * @returns {SmplElement} The current element.
	 */
	right() {
		return this.css({
			'margin-left': 'auto',
			'margin-right': 0,
		});
	}

	/**
	 * Disables the current element.
	 * @returns {SmplElement} The current element.
	 */
	disabled() {
		this._domElement.setAttribute('disabled', '');
		return this;
	}

	/**
	 * Enables the current element.
	 * @returns {SmplElement} The current element.
	 */
	enabled() {
		this._domElement.removeAttribute('disabled');
		return this;
	}

	/**
	 * Adds a class to the current element.
	 * @param {string} className - The class to add.
	 * @returns {SmplElement} The current element.
	 */
	addClass(className) {
		this._getOrCreateHtmlElement().classList.add(className);
		return this;
	}

	/**
	 * Removes a class from the current element.
	 * @param {string} className - The class to remove.
	 * @returns {SmplElement} The current element.
	 */
	removeClass(className) {
		this._getOrCreateHtmlElement().classList.remove(className);
		return this;
	}

	/**
	 * Toggles a class on the current element.
	 * @param {string} className - The class to toggle.
	 * @returns {SmplElement} The current element.
	 */
	toggleClass(className) {
		this._getOrCreateHtmlElement().classList.toggle(className);
		return this;
	}

	/**
	 * Sets the HTML content of the current element.
	 * @param {string} html - The HTML to set as the content of the element.
	 * @returns {SmplElement} The current element.
	 */
	html(html) {
		this._getOrCreateHtmlElement().innerHTML = html;
		return this;
	}

	/**
	 * Adds one or more classes to the current element.
	 * @param {string|string[]} classes - The class(es) to add.
	 * @returns {SmplElement} The current element.
	 */
	class(classes) {
		if (Array.isArray(classes)) {
			classes.forEach((c) => this._domElement.classList.add(c));
		} else if (typeof classes === 'string') {
			this._domElement.classList.add(classes);
		}
		return this;
	}

	/**
	 * Adds an event listener to the current element.
	 * @param {string} event - The event to listen for.
	 * @param {function} callback - The function to call when the event occurs.
	 * @returns {SmplElement} The current element.
	 */
	on(event, callback) {
		if (typeof callback === 'function') {
			this._domElement.addEventListener(event, (e) => {
				callback(e, this);
			});
		}
		return this;
	}

	/**
	 * Adds a 'click' event listener to the current element.
	 * @param {function} callback - The function to call when the 'click' event occurs.
	 * @returns {SmplElement} The current element.
	 */
	click(callback) {
		return this.on('click', callback);
	}

	/**
	 * Adds a 'dblclick' event listener to the current element.
	 * @param {function} callback - The function to call when the 'dblclick' event occurs.
	 * @returns {SmplElement} The current element.
	 */
	dblclick(callback) {
		return this.on('dblclick', callback);
	}

	/**
	 * Adds a 'mousedown' event listener to the current element.
	 * @param {function} callback - The function to call when the 'mousedown' event occurs.
	 * @returns {SmplElement} The current element.
	 */
	mousedown(callback) {
		return this.on('mousedown', callback);
	}

	/**
	 * Adds a 'mousemove' event listener to the current element.
	 * @param {function} callback - The function to call when the 'mousemove' event occurs.
	 * @returns {SmplElement} The current element.
	 */
	mousemove(callback) {
		return this.on('mousemove', callback);
	}

	/**
	 * Adds a 'mouseout' event listener to the current element.
	 * @param {function} callback - The function to call when the 'mouseout' event occurs.
	 * @returns {SmplElement} The current element.
	 */
	mouseout(callback) {
		return this.on('mouseout', callback);
	}

	/**
	 * Adds a 'mouseover' event listener to the current element.
	 * @param {function} callback - The function to call when the 'mouseover' event occurs.
	 * @returns {SmplElement} The current element.
	 */
	mouseover(callback) {
		return this.on('mouseover', callback);
	}

	/**
	 * Adds a 'mouseup' event listener to the current element.
	 * @param {function} callback - The function to call when the 'mouseup' event occurs.
	 * @returns {SmplElement} The current element.
	 */
	mouseup(callback) {
		return this.on('mouseup', callback);
	}

	/**
	 * Gets the parent element of the current element.
	 * @returns {HTMLElement} The parent element of the current element.
	 */
	getParent() {
		return this._domElement.parentElement;
	}

	/**
	 * Gets the child elements of the current element.
	 * @returns {HTMLElement[]} The child elements of the current element.
	 */
	getChildren() {
		return Array.from(this._domElement.children);
	}

	/**
	 * Gets the value of an attribute of the current element.
	 * @param {string} attribute - The name of the attribute to get.
	 * @returns {string} The value of the attribute.
	 */
	getAttribute(attribute) {
		return this._domElement.getAttribute(attribute);
	}

	/**
	 * Gets the DOM element for the current SmplElement.
	 * @returns {HTMLElement} The DOM element for the current SmplElement.
	 */
	getDomElement() {
		return this._domElement;
	}

	/**
	 * Inserts a label element with the specified text and options before the current element,
	 * and sets the 'for' attribute of the label to the 'id' attribute of the current element
	 * if it exists.
	 * @param {string} text - The text to set as the content of the label element.
	 * @param {object} options - Options for the label element.
	 * @returns {SmplElement} The current element.
	 */
	_insertLabelBefore(text, options) {
		if (text) {
			const labelEl = element('label', options).html(text, true).insertBefore(this);
			if (this._attributes?._id) {
				labelEl.attr('for', this._attributes._id);
			}
		}
	}

	/**
	 * Gets the HTML element for the current SmplElement, creating it if necessary.
	 * @returns {HTMLElement} The HTML element for the current SmplElement.
	 */
	_getOrCreateHtmlElement() {
		if (!this._domElement) {
			this._domElement = document.createElement(this._type);
			if (this._class) {
				this._domElement.className = this._class;
			}
			if (this._styles) {
				this.css(this._styles);
			}
			if (this._attributes) {
				this.attr(this._attributes);
			}
			if (this._events) {
				for (let key in this._events) {
					this._domElement.addEventListener(key, this._events[key]);
				}
			}
		}
		return this._domElement;
	}

	/**
	 * Adds an element to the current element.
	 * @param {SmplElement} element - The element to add.
	 */
	_addElement(element) {
		if (element instanceof SmplElement) {
			this._getOrCreateHtmlElement().appendChild(element._getOrCreateHtmlElement());
		}
	}

	/**
	 * Removes all child elements from the current element.
	 * @returns {SmplElement} The current element.
	 */
	clear() {
		removeChildren(this._getOrCreateHtmlElement());
		return this;
	}
}

export const element = (type, options) => new SmplElement(type, options);
