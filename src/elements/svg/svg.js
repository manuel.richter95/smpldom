import { SmplElement } from '../element.js';

export class Svg extends SmplElement {
	constructor(options) {
		super('svg', options);
	}
}

export function svg(options) {
	return new Svg(options);
}
