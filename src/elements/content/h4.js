import { SmplElement } from '../element.js';

export class H4 extends SmplElement {
	constructor(text, options) {
		super('h4', options);
		this._text = text;
		this.text(this._text);
	}
}

export function h4(text, options) {
	return new H4(text, options);
}
