import { SmplElement } from '../element.js';

export class Section extends SmplElement {
	constructor(options) {
		super('section', options);
	}
}

export function section(options) {
	return new Section(options);
}
