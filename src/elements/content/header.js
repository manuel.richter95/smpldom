import { SmplElement } from '../element.js';

export class Header extends SmplElement {
	constructor(options) {
		super('header', options);
	}
}

export function header(options) {
	return new Header(options);
}
