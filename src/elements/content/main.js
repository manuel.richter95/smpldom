import { SmplElement } from '../element.js';

export class Main extends SmplElement {
	constructor(options) {
		super('main', options);
	}
}

export function main(options) {
	return new Main(options);
}
