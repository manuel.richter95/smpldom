import { SmplElement } from '../element.js';

export class H3 extends SmplElement {
	constructor(text, options) {
		super('h3', options);
		this._text = text;
		this.text(this._text);
	}
}

export function h3(text, options) {
	return new H3(text, options);
}
