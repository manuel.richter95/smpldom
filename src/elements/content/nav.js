import { SmplElement } from '../element.js';

export class Nav extends SmplElement {
	constructor(options) {
		super('nav', options);
	}
}

export function nav(options) {
	return new Nav(options);
}
