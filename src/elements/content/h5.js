import { SmplElement } from '../element.js';

export class H5 extends SmplElement {
	constructor(text, options) {
		super('h5', options);
		this._text = text;
		this.text(this._text);
	}
}

export function h5(text, options) {
	return new H5(text, options);
}
