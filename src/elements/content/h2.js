import { SmplElement } from '../element.js';

export class H1 extends SmplElement {
	constructor(text, options) {
		super('h1', options);
		this._text = text;
		this.text(this._text);
	}
}

export function h1(text, options) {
	return new H1(text, options);
}
