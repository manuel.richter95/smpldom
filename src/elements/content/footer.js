import { SmplElement } from '../element.js';

export class Footer extends SmplElement {
	constructor(options) {
		super('footer', options);
	}
}

export function footer(options) {
	return new Footer(options);
}
