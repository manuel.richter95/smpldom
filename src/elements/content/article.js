import { SmplElement } from '../element.js';

export class Article extends SmplElement {
	constructor(options) {
		super('article', options);
	}
}

export function article(options) {
	return new Article(options);
}
