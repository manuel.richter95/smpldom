import { SmplElement } from '../element.js';

export class H6 extends SmplElement {
	constructor(text, options) {
		super('h6', options);
		this._text = text;
		this.text(this._text);
	}
}

export function h6(text, options) {
	return new H6(text, options);
}
