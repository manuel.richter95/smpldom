import { SmplElement } from '../element.js';

export class Th extends SmplElement {
	constructor(text, options) {
		super('th', options);
		this._text = text;
		this.text(this._text);
	}
}

export function th(text, options) {
	return new Th(text, options);
}
