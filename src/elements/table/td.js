import { SmplElement } from '../element.js';

export class Td extends SmplElement {
	constructor(text, options) {
		super('td', options);
		this._text = text;
		this.text(this._text);
	}

	colspan(span) {
		return this.attr('colspan', span);
	}

	rowspan(span) {
		return this.attr('rowspan', span);
	}
}

export function td(text, options) {
	return new Td(text, options);
}
