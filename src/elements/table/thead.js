import { SmplElement } from '../element.js';
import { Td } from './td.js';
import { th, Th } from './th.js';
import { tr } from './tr.js';

export class Thead extends SmplElement {
	constructor(options) {
		super('thead', options);
	}

	tr(...elements) {
		const tblTr = tr();
		this.add(tblTr);
		elements.forEach((el) => {
			if (el instanceof Td) {
				tblTr.add(el);
			} else if (el instanceof Th) {
				this.add(el);
				tblTr.remove();
			} else if (typeof el === 'string') {
				tblTr.add(th(el));
			}
		});
		return this;
	}
}

export function thead(options) {
	return new Thead(options);
}
