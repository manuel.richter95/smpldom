import { SmplElement } from '../element.js';
import { td, Td } from './td.js';
import { Tr, tr } from './tr.js';

export class Tbody extends SmplElement {
	constructor(options) {
		super('tbody', options);
	}

	tr(...elements) {
		const tblTr = tr();
		this.add(tblTr);
		elements.forEach((el) => {
			if (el instanceof Td) {
				tblTr.add(el);
			} else if (el instanceof Tr) {
				this.add(el);
				tblTr.remove();
			} else if (typeof el === 'string') {
				tblTr.add(td(el));
			}
		});
		return this;
	}
}

export function tbody(options) {
	return new Tbody(options);
}
