import { SmplElement } from '../element.js';
import { td, Td } from './td.js';
import { th, Th } from './th.js';
import { thead } from './thead.js';
import { tr, Tr } from './tr.js';

export class Table extends SmplElement {
	constructor(options) {
		super('table', options);
	}

	th(...elements) {
		elements.forEach((el) => {
			if (el instanceof Th) {
				this.add(el);
			} else if (typeof el === 'string') {
				this.add(th(el));
			}
		});
		return this;
	}

	tr(...elements) {
		const tblTr = tr();
		this.add(tblTr);
		elements.forEach((el) => {
			if (el instanceof Td) {
				tblTr.add(el);
			} else if (el instanceof Tr) {
				this.add(el);
				tblTr.remove();
			} else if (typeof el === 'string') {
				tblTr.add(td(el));
			}
		});
		return this;
	}
}

export function table(options) {
	return new Table(options);
}
