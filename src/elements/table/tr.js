import { SmplElement } from '../element.js';
import { td } from './td.js';
import { th } from './th.js';

export class Tr extends SmplElement {
	constructor(options) {
		super('tr', options);
	}

	td(text, options) {
		return this.add(td(text, options));
	}
	th(text, options) {
		return this.add(th(text, options));
	}
}

export function tr(options) {
	return new Tr(options);
}
