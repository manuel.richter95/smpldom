import { SmplElement } from '../element.js';

export class Img extends SmplElement {
	constructor(src, options) {
		super('img', options);
		this._src = src;
		this.src(this._src);
	}

	src(url, alt) {
		this.getDomElement().src = url;
		if (alt) this.alt(alt);
	}
	alt(name) {
		this.getDomElement().alt = name;
	}
	crossOrigin(origin) {
		this.getDomElement().crossOrigin = origin;
	}
}

export function img(src, options) {
	return new Img(src, options);
}
