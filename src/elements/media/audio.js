import { SmplElement } from '../element.js';

export class Audio extends SmplElement {
	constructor(options) {
		super('audio', options);
	}
}

export function audio(options) {
	return new Audio(options);
}
