import { SmplElement } from '../element.js';

export class Video extends SmplElement {
	constructor(options) {
		super('video', options);
	}
}

export function video(options) {
	return new Video(options);
}
