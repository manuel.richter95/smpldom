import { SmplElement } from '../element.js';
import { li } from './li.js';

export class Ol extends SmplElement {
	constructor(options) {
		super('ol', options);
	}

	li(text, options) {
		return this.add(li(text, options));
	}
}

export function ol(options) {
	return new Ol(options);
}
