import { SmplElement } from '../element.js';

export class P extends SmplElement {
	constructor(text, options) {
		super('p', options);
		this._text = text;
		this.text(this._text);
	}
}

export function p(text, options) {
	return new P(text, options);
}
