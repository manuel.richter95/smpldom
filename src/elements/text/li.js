import { SmplElement } from '../element.js';

export class Li extends SmplElement {
	constructor(text, options) {
		super('li', options);
		this._text = text;
		this.text(this._text);
	}
}

export function li(text, options) {
	return new Li(text, options);
}
