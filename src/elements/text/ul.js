import { SmplElement } from '../element.js';
import { li } from './li.js';

export class Ul extends SmplElement {
	constructor(options) {
		super('ul', options);
	}

	li(text, options) {
		return this.add(li(text, options));
	}
}

export function ul(options) {
	return new Ul(options);
}
