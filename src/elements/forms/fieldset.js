import { SmplElement } from '../element.js';

export class Fieldset extends SmplElement {
	constructor(options) {
		super('fieldset', options);
	}
}

export function fieldset(options) {
	return new Fieldset(options);
}
