import { SmplElement } from '../element.js';

export class Button extends SmplElement {
	constructor(text, options) {
		super('button', options);
		this._text = text;
		this.text(this._text);
	}
}

export function button(text, options) {
	return new Button(text, options);
}
