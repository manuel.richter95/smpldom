import { element, SmplElement } from '../element.js';

export class Select extends SmplElement {
	constructor(options) {
		super('select', options);
	}

	label(text, options) {
		this._insertLabelBefore(text, options);
		return this;
	}

	option(value, label, options) {
		return this.add(element('option', options).text(label).attr('value', value));
	}

	multiple() {
		return this.attr('multiple', '');
	}
	size(size) {
		return this.attr('size', size);
	}
}

export function select(options) {
	return new Select(options);
}
