import { SmplElement, element } from '../element.js';

export class Textarea extends SmplElement {
	constructor(options) {
		super('textarea', options);
	}

	label(text, options) {
		this._insertLabelBefore(text, options);
		return this;
	}

	rows(rows) {
		this.attr('rows', rows);
		return this;
	}
	cols(cols) {
		this.attr('cols', cols);
		return this;
	}
	keydown(callback) {
		return this.on('keydown', callback);
	}
	keypress(callback) {
		return this.on('keypress', callback);
	}
	keyup(callback) {
		return this.on('keyup', callback);
	}
}

export function textarea(options) {
	return new Textarea(options);
}
