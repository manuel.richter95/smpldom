import { SmplElement } from '../element.js';

export class Form extends SmplElement {
	constructor(options) {
		super('form', options);
	}

	submit(callback) {
		return this.on('submit', callback);
	}
}

export function form(options) {
	return new Form(options);
}
