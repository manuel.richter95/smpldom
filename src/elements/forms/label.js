import { SmplElement } from '../element.js';

export class Label extends SmplElement {
	constructor(text, options) {
		super('label', options);
		this._text = text;
		this.text(this._text);
	}
}

export function label(text, options) {
	return new Label(text, options);
}
