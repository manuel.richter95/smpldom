import { SmplElement } from '../element.js';

export class Legend extends SmplElement {
	constructor(text, options) {
		super('legend', options);
		this._text = text;
		this.text(this._text);
	}
}

export function legend(text, options) {
	return new Legend(text, options);
}
