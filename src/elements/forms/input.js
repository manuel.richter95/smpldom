import { SmplElement } from '../element.js';

export class Input extends SmplElement {
	constructor(type, options) {
		super('input', options);
		this._itype = type;
		this.attr('type', this._itype);
	}

	label(text, options) {
		this._insertLabelBefore(text, options);
		return this;
	}
	keydown(callback) {
		return this.on('keydown', callback);
	}
	keypress(callback) {
		return this.on('keypress', callback);
	}
	keyup(callback) {
		return this.on('keyup', callback);
	}
}

export function input(type, options) {
	return new Input(type, options);
}
