import { SmplElement } from '../element.js';

export class Link extends SmplElement {
	constructor(options) {
		super('link', options);
	}

	href(url) {
		this.getDomElement().href = url;
		return this;
	}

	rel(relationship) {
		this.attr('rel', relationship);
		return this;
	}
}

export function link(options) {
	return new Link(options);
}
