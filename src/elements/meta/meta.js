import { SmplElement } from '../element.js';

export class Meta extends SmplElement {
	constructor(options) {
		super('meta', options);
	}
}

export function meta(options) {
	return new Meta(options);
}
