import { SmplElement } from '../element.js';

export class Head extends SmplElement {
	constructor(options) {
		super('head', options);
	}
}

export function Head(options) {
	return new head(options);
}
