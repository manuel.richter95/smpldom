import { SmplElement } from '../element.js';

export class Style extends SmplElement {
	constructor(options) {
		super('style', options);
	}
}

export function style(options) {
	return new Style(options);
}
