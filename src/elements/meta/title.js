import { SmplElement } from '../element.js';

export class Title extends SmplElement {
	constructor(options) {
		super('title', options);
	}
}

export function title(options) {
	return new Title(options);
}
