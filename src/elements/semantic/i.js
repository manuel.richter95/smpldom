import { SmplElement } from '../element.js';

export class I extends SmplElement {
	constructor(className, options) {
		super('i', options);
		this.class(className);
	}
}

export function i(className, options) {
	return new I(className, options);
}
