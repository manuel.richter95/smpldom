import { SmplElement } from '../element.js';

export class Div extends SmplElement {
	constructor(options) {
		super('div', options);
	}
}

export function div(options) {
	return new Div(options);
}
