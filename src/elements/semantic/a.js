import { SmplElement } from '../element.js';

export class A extends SmplElement {
	constructor(href, text, options) {
		super('a', options);
		this._href = href;
		this.href(this._href);
		this.text(text);
	}

	href(url) {
		this.getDomElement().href = url;
		return this;
	}
}

export function a(href, text, options) {
	return new A(href, text, options);
}
