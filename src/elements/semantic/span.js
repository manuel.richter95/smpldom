import { SmplElement } from '../element.js';

export class Span extends SmplElement {
	constructor(options) {
		super('span', options);
	}
}

export function span(options) {
	return new Span(options);
}
