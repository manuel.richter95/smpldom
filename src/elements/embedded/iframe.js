import { SmplElement } from '../element.js';

export class Iframe extends SmplElement {
	constructor(options) {
		super('iframe', options);
	}
}

export function iframe(options) {
	return new Iframe(options);
}
