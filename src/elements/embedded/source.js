import { SmplElement } from '../element.js';

export class Source extends SmplElement {
	constructor(options) {
		super('source', options);
	}
}

export function source(options) {
	return new Source(options);
}
