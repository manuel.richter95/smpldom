# smpldom

A library to add or manipulate HTML/DOM elements

# Installation (not yet published to npm!)

```bash
npm install smpldom
```

# Usage

```javascript
import { element, input } from 'smpldom';

//create a unspecific element with an options object
element('input', {
	class: 'foo', //or array of strings
	attributes: {
		disabled: '',
		type: 'text',
	},
	id: 'my-input',
	styles: {
		width: '100px',
		height: '30px',
		color: 'red',
	},
	events: {
		keydown: (event, element) => console.log(event, element),
	},
});

//create the same but with methods
input('text')
	.class('foo')
	.disabled() //or .attr("disabled")
	.id('my-input')
	.css({
		width: '100px',
		color: 'red',
	}) //or .css(key, value)
	.height('30px') //width is also possible
	.keydown((event, element) => console.log(event, element)) //or .on("keydown", callback)
	.append(document.body); //this is the default, you can also use prepend(element), insertAfter(element), inserBefore(element)
```

this appends the following to the body:

```html
<input type="text" class="foo" disabled id="my-input" style="width: 100px; color: red; height: 30px;" />
```

## Create a table multiple ways

```javascript
const tbl = table()
	.css('border', '1px solid black')
	.add(thead().add(tr().th('header1').th('header2')))
	.add(tbody().add(tr().td('data1').td('data2'), tr().td('data3').td('data4')))
	.add(tfoot().add(td('footer').colspan(2).textAlign('center')));
```

```html
<table style="border: 1px solid black;">
	<thead>
		<tr>
			<th>header1</th>
			<th>header2</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td>data1</td>
			<td>data2</td>
		</tr>
		<tr>
			<td>data3</td>
			<td>data4</td>
		</tr>
	</tbody>
	<tfoot>
		<td colspan="2" style="text-align: center;">footer</td>
	</tfoot>
</table>
```

```javascript
//these 4 will output the same
const tbl2 = table().th('header1').th('header2');
tbl2.tr('data1', 'data2');

const tbl3 = table().add(th('header1'), th('header2'));
tbl3.tr(td('data1'), td('data2'));

const tbl4 = table().th(th('header1'), th('header2'));
tbl4.tr(td('data1'), td('data2'));

const tbl5 = table().th(th('header1'), th('header2'));
tbl5.tr(tr().td('data1').td('data2'));
```

```html
<table>
	<th>header1</th>
	<th>header2</th>
	<tr>
		<td>data1</td>
		<td>data2</td>
	</tr>
</table>
```

## More examples

```javascript
//add a ul with two list items
ul({ class: 'list' })
	.li('text1', { styles: { color: 'red' } })
	.li('text2');

div()
	.label('foo') //creates <label>foo</label>
	.right() //aligns the div to the right of its parent
	.add(button('click').click(() => console.log('clicked')));

form()
	.add(input('text', 'username'), input('password', 'password'), input('submit').value('click me!'))
	.on('submit', () => alert('submitted'));
```
